package example.com.sensorinfo;

import android.hardware.Sensor;

import java.util.HashMap;

/**
 * Created by bharat on 1/7/2014.
 */
public class Model {
    private Sensor sensor;
    private boolean selected;


    // hashmap based model
    int sensorCount;
    String sensorName;
    String sensorVendor;
    double sensorRange;
    int sensorDelay;
    double sensorPower;
    double sensorResolution;
    int sensorType;
    int sensorVersion;

    // ends hashmap based model


    public Model(HashMap map){
        this.sensor = null;
        this.sensorCount = (Integer) map.get("SensorId");
        this.sensorName = (String) map.get("SensorName");
        this.sensorVendor = (String) map.get("SensorVendor");
        this.sensorRange = map.get("SensorMaxRange").getClass().getName().equals(Double.class.getName()) ? (Double) map.get("SensorMaxRange") : (Integer) map.get("SensorMaxRange");
        this.sensorDelay = (Integer) map.get("SensorMinDelay");
        this.sensorPower = map.get("SensorPower").getClass().getName().equals(Double.class.getName()) ? (Double) map.get("SensorPower") : (Integer) map.get("SensorPower");
        this.sensorResolution = map.get("SensorResolution").getClass().getName().equals(Double.class.getName()) ? (Double) map.get("SensorResolution") : (Integer) map.get("SensorResolution");
        this.sensorType = (Integer) map.get("SensorType");
        this.sensorVersion = (Integer) map.get("SensorVersion");
    }

    public Model(Sensor sensor) {
        this.sensor = sensor;
        selected = false;
    }

    public Sensor getInfo() {
        return sensor;
    }

    public String getText(){
        if(sensor == null){
            return "Name : " + sensorName + "\n"
                    + "Vendor : " + sensorVendor + "\n"
                    + "Range : " + sensorRange + "\n"
                    + "MinDelay : " + sensorDelay + "\n"
                    + "Power : " + sensorPower + "\n"
                    + "Resolution : " + sensorResolution + "\n"
                    + "Type : " + sensorType + "\n"
                    + "Version : " + sensorVersion;
        }else {
            return "Name : " + sensor.getName() + "\n"
                    + "Vendor : " + sensor.getVendor() + "\n"
                    + "Range : " + sensor.getMaximumRange() + "\n"
                    + "MinDelay : " + sensor.getMinDelay() + "\n"
                    + "Power : " + sensor.getPower() + "\n"
                    + "Resolution : " + sensor.getResolution() + "\n"
                    + "Type : " + sensor.getType() + "\n"
                    + "Version : " + sensor.getVersion();
        }
    }

    public void setInfo(Sensor sensor) {
        this.sensor = sensor;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
