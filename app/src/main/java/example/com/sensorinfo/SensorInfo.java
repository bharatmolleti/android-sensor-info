package example.com.sensorinfo;

// basic sensor info.

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


public class SensorInfo extends Activity {
    TextView mBox;

    List<ToggleButton> mButtons;
    Context mContext;
    ScrollView mView;

    ArrayAdapter<Model> adapter;

    private String android_id;
    private TabHost mTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_info);

        mTabHost = (TabHost) this.findViewById(R.id.tabHost);
        mTabHost.setup();


        TabHost.TabSpec ts1 = mTabHost.newTabSpec("TAB_TAG_1");
        ts1.setIndicator("Your Device");
        ts1.setContent(new TabHost.TabContentFactory() {

            public View createTabContent(String tag) {
                //ArrayAdapter<String> adapter = new ArrayAdapter<String>(Main_screen.this,android.R.layout.simple_list_item_1,new String[]{"item1","item2","item3"});
                SensorManager mSensorManager;

                mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

                List<Sensor> deviceSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);

                adapter = new InteractiveArrayAdapter(SensorInfo.this, getModel(deviceSensors));

                ListView listView;// = (ListView)findViewById(R.id.sensor_info);
                listView = new ListView(SensorInfo.this);
                //listView.addFooterView(footerView);
                listView.setAdapter(adapter);
                return listView;
            }
        });
        mTabHost.addTab(ts1);

        //LinearLayout footerView = (LinearLayout) getLayoutInflater().inflate(R.layout.footerview, null);
        TabHost.TabSpec ts2 = mTabHost.newTabSpec("TAB_TAG_2");
        ts2.setIndicator("+");
        ts2.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                //ArrayAdapter<String> adapter = new ArrayAdapter<String>(Main_screen.this,android.R.layout.simple_list_item_1,new String[]{"item1","item2","item3"});
                SensorManager mSensorManager;

                mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

                List<Sensor> deviceSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);

                adapter = new InteractiveArrayAdapter(SensorInfo.this, getModel(deviceSensors));

                // Creating a new RelativeLayout
                LinearLayout tabLayout = new LinearLayout(SensorInfo.this);
                tabLayout.setOrientation(LinearLayout.VERTICAL);

                ListView listView;// = (ListView)findViewById(R.id.sensor_info);
                listView = new ListView(SensorInfo.this);
                listView.setAdapter(adapter);

                RelativeLayout footerView = (RelativeLayout) getLayoutInflater().inflate(R.layout.footerview, null);

                tabLayout.addView(footerView);
                tabLayout.addView(listView);

                return tabLayout;
            }
        });

        mTabHost.addTab(ts2);

        Parse.initialize(this, "qjc8sLaCM3JUcf5klWZBa4HJ0BNTDmNcT1hEjTd5", "IT9vJpYOMFvhO9bmx6vn9ab6FZ0rDEVYqdWL2QNT");
        android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("SensorInfo", "Signing in with AndroidId " + android_id);
        new ParseCloudTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sensor_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_graph) {
            Intent i = new Intent(this, GraphActivity.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private Model get(Sensor s) {
        return new Model(s);
    }

    private List<Model> getModel(List<Sensor> deviceSensors) {
        List<Model> list = new ArrayList<Model>();

        for (Sensor sensor : deviceSensors) {
            list.add(get(sensor));
        }

        // Initially select one of the items
        list.get(1).setSelected(false);
        return list;
    }


    private Model parseHashmap(HashMap s) {
        return new Model(s);
    }

    private List<Model> getModelFromParseData(ArrayList sensorList){
        List<Model> list = new ArrayList<Model>();

        for (Object pObj : sensorList) {
            HashMap pnObj = (HashMap) pObj;
            list.add(parseHashmap(pnObj));
        }

        // Initially select one of the items
        list.get(1).setSelected(false);

        return list;
    }

    public String getUsername() {
        AccountManager manager = AccountManager.get(this);
        Account[] accounts = manager.getAccountsByType("com.google");
        List<String> possibleEmails = new LinkedList<String>();

        for (Account account : accounts) {
            // TODO: Check possibleEmail against an email regex or treat
            // account.name as an email address only for certain account.type values.
            possibleEmails.add(account.name);
            // add the device data to the online cloud database..
        }

        if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
            String email = possibleEmails.get(0);
            String[] parts = email.split("@");
            if (parts.length > 0 && parts[0] != null)
                return parts[0];
            else
                return null;
        } else
            return null;
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    private class ParseCloudTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... voids) {
            // new parse cloud cache stuff
            ParseUser currentUser = ParseUser.getCurrentUser();
            if (currentUser != null) {
                // do stuff with the user
                Log.d("SensorInfo", "User has been Cached, and has been logged in automatically");
                // instead retrieve data for comparison.
            } else {
                // show the signup or login screen
                String userName = android_id;
                Log.d("SensorInfo", "Attempting to sign in new User with username: " + userName);

                ParseQuery query = ParseUser.getQuery();
                query.whereEqualTo("username", userName);
                query.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> scoreList, ParseException e) {
                        if (e == null) {
                            Log.d("SensorInfo", "This Device already has been updated in the Database, will sign in instead");
                            String userName = android_id;
                            Log.d("SensorInfo", "Attempting to log in existing User with username: " + userName);
                            ParseUser.logInInBackground(userName, "password", new LogInCallback() {
                                public void done(ParseUser user, ParseException e) {
                                    if (user != null) {
                                        Log.d("SensorInfo", "User Sign In Succeeded ");
                                    } else {
                                        Log.e("SensorInfo", "User Sign in Failed");
                                    }
                                }
                            });
                        } else {
                            Log.d("SensorInfo", "Confirmed New Device " + e);
                            String userName = android_id;
                            Log.d("SensorInfo", "New User with username: " + userName);
                            ParseUser user = new ParseUser();
                            user.setUsername(userName);
                            user.setPassword("password");
                            user.signUpInBackground(new SignUpCallback() {
                                public void done(ParseException e) {
                                    if (e == null) {
                                        // Hooray! Let them use the app now.
                                        Log.d("SensorInfo", "New User has been Signed in");
                                        // Upload the device info
                                        SensorManager mSensorManager;

                                        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

                                        List<Sensor> deviceSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
                                        ParseObject testObject = new ParseObject("SensorsList");

                                        testObject.put("UserName", android_id);
                                        testObject.put("DeviceName", getDeviceName());

                                        JSONArray jsonArray = new JSONArray();

                                        int nCount = 0;
                                        for (Sensor sensor : deviceSensors) {
                                            JSONObject pnObj = new JSONObject();
                                            try {
                                                pnObj.put("SensorId", nCount);
                                                Log.d("UploadSensor", nCount + ", Name : " + sensor.getName());
                                                pnObj.put("SensorName", sensor.getName());
                                                Log.d("UploadSensor", nCount + ", Vendor : " + sensor.getVendor());
                                                pnObj.put("SensorVendor", sensor.getVendor());
                                                Log.d("UploadSensor", nCount + ", Range : " + sensor.getMaximumRange());
                                                pnObj.put("SensorMaxRange", sensor.getMaximumRange());
                                                Log.d("UploadSensor", nCount + ", MinDelay : " + sensor.getMinDelay());
                                                pnObj.put("SensorMinDelay", sensor.getMinDelay());
                                                Log.d("UploadSensor", nCount + ", Power : " + sensor.getPower());
                                                pnObj.put("SensorPower", sensor.getPower());
                                                Log.d("UploadSensor", nCount + ", Resolution : " + sensor.getResolution());
                                                pnObj.put("SensorResolution", sensor.getResolution());
                                                Log.d("UploadSensor", nCount + ", Type : " + sensor.getType());
                                                pnObj.put("SensorType", sensor.getType());
                                                Log.d("UploadSensor", nCount + ", Version : " + sensor.getVersion());
                                                pnObj.put("SensorVersion", sensor.getVersion());
                                                jsonArray.put(pnObj);
                                            } catch (JSONException e1) {
                                                e1.printStackTrace();
                                            }
                                            nCount++;
                                        }
                                        testObject.put("SensorArray", jsonArray);
                                        testObject.put("TotalSensors", nCount);
                                        Log.d("UploadSensor", "Uploaded a Total Of " + nCount + " Sensors Data");
                                        testObject.saveInBackground();

                                    } else {
                                        // Sign up didn't succeed. Look at the ParseException
                                        // to figure out what went wrong
                                        Log.d("SensorInfo", "User Sign in Failed with " + e);
                                    }
                                }
                            });
                        }
                    }
                });
            }
            return null;
        }

        protected void onPostExecute(Void code) {
            // fine with the user login-validation stuff, lets play with queries.
            // get the data from the server, and dynamically generate and populate tabs.
            ParseQuery<ParseObject> query = ParseQuery.getQuery("SensorsList");
            //query.whereEqualTo("DeviceName", "Samsung GT-I9100");
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> deviceList, ParseException e) {
                    if (e == null) {
                        Log.d("score", "Retrieved " + deviceList.size() + " Devices Sensor Details");
                        int nDeviceCount = 0;
                        Log.d("SensorInfo","=======================================================================");
                        for (ParseObject device : deviceList) {
                            String sID = device.getString("UserName");
                            String sDeviceName = device.getString("DeviceName");

                            int nCount = device.getInt("TotalSensors");

                            final ArrayList jsonArray = (ArrayList) device.get("SensorArray");
                            int i = 0;
                            for (Object pObj : jsonArray) {
                                HashMap pnObj = (HashMap) pObj;
                                //JSONObject pnObj = jsonArray.getJSONObject(i);
                                //JSONObject pnObj = (JSONObject)pObj;
                                int sensorCount = (Integer) pnObj.get("SensorId");
                                String sensorName = (String) pnObj.get("SensorName");
                                String sensorVendor = (String) pnObj.get("SensorVendor");
                                double sensorRange = pnObj.get("SensorMaxRange").getClass().getName().equals(java.lang.Double.class.getName()) ? (Double) pnObj.get("SensorMaxRange") : (Integer) pnObj.get("SensorMaxRange");
                                int sensorDelay = (Integer) pnObj.get("SensorMinDelay");
                                double sensorPower = pnObj.get("SensorPower").getClass().getName().equals(java.lang.Double.class.getName()) ? (Double) pnObj.get("SensorPower") : (Integer) pnObj.get("SensorPower");
                                double sensorResolution = pnObj.get("SensorResolution").getClass().getName().equals(java.lang.Double.class.getName()) ? (Double) pnObj.get("SensorResolution") : (Integer) pnObj.get("SensorResolution");
                                int sensorType = (Integer) pnObj.get("SensorType");
                                int sensorVersion = (Integer) pnObj.get("SensorVersion");

                                Log.d("SensorInfo", "_____________________________________________________________");
                                Log.d("SensorInfo", "For Count: " + i + " , Sensor Id : " + sensorCount);
                                Log.d("SensorInfo", " Name: " + sensorName + " , Vendor " + sensorVendor);
                                Log.d("SensorInfo", " Range: " + sensorRange + " , Delay " + sensorDelay);
                                Log.d("SensorInfo", " Power: " + sensorPower + " , Resolution " + sensorResolution);
                                Log.d("SensorInfo", " Type: " + sensorType + " , Version " + sensorVersion);
                                Log.d("SensorInfo", "_____________________________________________________________");
                                i++;
                            }
                            nDeviceCount++;

                            TabHost.TabSpec ts = mTabHost.newTabSpec("TAB_TAG_"+(2+nDeviceCount));
                            ts.setIndicator(sDeviceName);

                            ts.setContent(new TabHost.TabContentFactory() {
                                public View createTabContent(String tag) {
                                    //ArrayAdapter<String> adapter = new ArrayAdapter<String>(Main_screen.this,android.R.layout.simple_list_item_1,new String[]{"item1","item2","item3"});
                                    //SensorManager mSensorManager;

                                    //mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

                                   // List<Sensor> deviceSensors = jsonArray;//mSensorManager.getSensorList(Sensor.TYPE_ALL);

                                    //String sID = device.getString("UserName");

                                    adapter = new InteractiveArrayAdapter(SensorInfo.this, getModelFromParseData(jsonArray));

                                    // Creating a new RelativeLayout
                                    LinearLayout tabLayout = new LinearLayout(SensorInfo.this);
                                    tabLayout.setOrientation(LinearLayout.VERTICAL);

                                    ListView listView;// = (ListView)findViewById(R.id.sensor_info);
                                    listView = new ListView(SensorInfo.this);
                                    listView.setAdapter(adapter);

                                    RelativeLayout footerView = (RelativeLayout) getLayoutInflater().inflate(R.layout.footerview, null);

                                    tabLayout.addView(footerView);
                                    tabLayout.addView(listView);

                                    return tabLayout;
                                }
                            });

                            mTabHost.addTab(ts);

                        }
                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });

        }
    }
}
